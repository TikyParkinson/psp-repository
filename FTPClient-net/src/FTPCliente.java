
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;
import java.util.Scanner;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

public class FTPCliente {

	public static void main(String[] args) throws SocketException, IOException {
		FTPClient clienteFTP = new FTPClient();
		String serverFTP = "ftp.rediris.es";
		int responseFTP;

		System.out.println("Conectando a "+serverFTP);
		clienteFTP.connect(serverFTP, 21);
		responseFTP = clienteFTP.getReplyCode();
		System.out.println("Codigo :"+responseFTP);

		clienteFTP.login("master","pepe123");
		System.out.println(clienteFTP.getReplyString());
		String workingDir = "/";

		String tipoArchivo [] = {"file","directory","sym link"};
		clienteFTP.changeWorkingDirectory(workingDir);
		FTPFile [] carpeta =  clienteFTP.listFiles();

		System.out.println("Listado del directorio actual");
		Scanner sc = new Scanner(System.in);
		String comando;

		listaFiles(carpeta,tipoArchivo);
		System.out.println("Comando :");
		comando = sc.nextLine();

		while (true) {

			if (comando.contains("q")) {
				break;
			}

			if (comando.contains("download")){
			   int nFile = sc.nextInt();
			   String file2Download = carpeta[nFile].getName();
			   String fileLocal = carpeta[nFile].getName();
			   BufferedOutputStream descarga = new BufferedOutputStream( 
						new FileOutputStream(fileLocal) );

				boolean res = clienteFTP.retrieveFile(file2Download, descarga);

				if (res) {
					System.out.println("descarga correcta");
				}else {
					System.out.println("descarga incorrecta");
				}//end-if

				descarga.close();
			}//end-download
			if (comando.contains("list")){
				listaFiles(carpeta,tipoArchivo);
			}
			System.out.println("Comando :");
			comando = sc.next();
		}
		/*
		String file2Download = "test02";
		String fileLocal = "test02";
		BufferedOutputStream descarga = new BufferedOutputStream( 
				new FileOutputStream(fileLocal) );
		boolean res = clienteFTP.retrieveFile(file2Download, descarga);
		if (res) {
			System.out.println("descarga correcta");
		}else {
			System.out.println("descarga incorrecta");
		}//end-if
		descarga.close();
		*/
	}

	private static void listaFiles(FTPFile[] carpeta, String[] tipoArchivo) {
		for (int i=0; i < carpeta.length; i++) {
			if (carpeta[i].getType() == 0)
				System.out.println(i + " "+carpeta[i].getName() + " > " + tipoArchivo[carpeta[i].getType()]);
		}
	}

}
