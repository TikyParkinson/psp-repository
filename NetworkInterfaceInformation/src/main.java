package main;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Enumeration;

public class main {

    public static void main(String[] args) {

        InetAddress equipo;

        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            for (NetworkInterface miInterface : Collections.list(interfaces)) {
                displayInterfaceInformacion(miInterface);
            }
        } catch (SocketException e) {
        }


    }

    private static void displayInterfaceInformacion(NetworkInterface miInterface) throws SocketException {
        System.out.println("Nombre :"+miInterface.getDisplayName() );
        if (!miInterface.getDisplayName().contains("ethernet")) {
            byte[] mac = miInterface.getHardwareAddress();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++)
                sb.append(((String.format("%02X%s", mac[i], (i<mac.length-1) ? ":":""))));
            System.out.println("MAC "+sb.toString());
        }
    }

}

