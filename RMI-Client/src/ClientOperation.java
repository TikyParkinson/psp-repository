import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JOptionPane;

public class ClientOperation {

	public static void main(String[] args) throws RemoteException, MalformedURLException, NotBoundException{
		RMIInterface conector = (RMIInterface) Naming.lookup("//localhost/Server");
		String nombre = JOptionPane.showInputDialog("Dime tu nombre");
		
		String respuesta = conector.hellowTo(nombre);
		JOptionPane.showMessageDialog(null, respuesta);
	}

}
