import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIInterface extends Remote  {
	public String hellowTo(String name) throws RemoteException;

}
