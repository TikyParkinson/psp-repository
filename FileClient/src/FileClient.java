import java.io.*;
import java.net.Socket;

public class FileClient {
    public static void main(String[] args) throws IOException {
        final String SERVER_IP = "127.0.0.1";
        final int SERVER_PORT = 4000;
        final String FICHERO = "fichero.txt";
        final int FICHERO_MAX = 104857600;// 100MiB

        int bytesLeidos;
        File ficheroRecibir = new File(FICHERO);
        FileOutputStream fos;
        BufferedOutputStream bos;

        InputStream is;
        Socket s = null;
        byte[] buffer = new byte[FICHERO_MAX];

        // Recibir archivo
        s = new Socket(SERVER_IP, SERVER_PORT);
        is = s.getInputStream();

        // Abrir fichero local a grabar
        fos = new FileOutputStream(ficheroRecibir);
        bos = new BufferedOutputStream(fos);

        // Leer bytes del socket
        bytesLeidos = is.read(buffer, 0, buffer.length); // Solo lee el primer byte
        int actual = bytesLeidos;
        while (bytesLeidos > -1) {
            bytesLeidos = is.read(buffer, actual, (buffer.length - actual));
            if (bytesLeidos >= 0) {
                actual += bytesLeidos;
            }
        }

        // Grabar en fichero
        bos.write(buffer, 0, actual);
        bos.flush();
        System.out.println(String.format("Fichero %s descargado %d bytes leidos.", FICHERO, actual));
        bos.close();
        fos.close();
        s.close();
    }
}
