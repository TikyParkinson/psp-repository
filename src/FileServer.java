/**
 * @author - Antonio Ruiz Marin
 * @version - 0.0.001-alpha
 * @date - 13/12/19
 * @since - 0.0.001-alpha
 */
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class FileServer {
    static int conectados;

    public static void main(String[] args) throws IOException {
        final int CLIENTES_MAX = 3;
        final int SERVER_PORT = 4000;
        int cliente  = 0;
        ServerSocket server = new ServerSocket(SERVER_PORT);

        System.out.println("Esperando conexiones de clientes");
        while(true){
            Socket s = server.accept();
            if (conectados < CLIENTES_MAX) {
                FileService service = new FileService(s,cliente);
                conectados++;
                cliente++;
                System.out.println("Hay "+conectados+" clientes conectados");
            }
            else {
                s.close();
            }

        }

    }

}
