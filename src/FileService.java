/**
 * @author - Antonio Ruiz Marin
 * @version - 0.0.001-alpha
 * @date - 13/12/19
 * @since - 0.0.001-alpha
 */
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class FileService extends Thread{
    Socket s;
    int cliente;
    Scanner in;
    PrintWriter out;

    File ficheroEnviar;
    FileInputStream fis;
    BufferedInputStream bis;
    OutputStream os;

    public FileService(Socket s, int cliente) {
        this.s = s;
        this.cliente = cliente;
        this.ficheroEnviar = new File("Odoo13.txt");
        this.start();
    }

    public void run() {
        System.out.println("El cliente "+cliente+" se ha conectado");

        try {
            byte [] sDatos = new byte [(int) ficheroEnviar.length()];
            fis = new FileInputStream(ficheroEnviar);
            bis = new BufferedInputStream(fis);

            //bis.read(sDatos,0,sDatos.length);
            bis.read(sDatos);
            os = s.getOutputStream();
            System.out.println("Enviando archivo..." + ficheroEnviar + " " + sDatos.length + " bytes");

            //os.write(sDatos,0,sDatos.length);
            os.write(sDatos);
            os.flush();
            os.close();
            s.close();

            FileServer.conectados--;
            System.out.println("Archivo enviado...");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }//end-run
}
