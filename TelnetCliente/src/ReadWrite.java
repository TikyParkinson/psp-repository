import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import org.apache.commons.net.io.Util;

public class ReadWrite {

	private InputStream  remoteInput;
	private OutputStream remoteOutput;
	private InputStream  localInput;
	private OutputStream localOutput;
	Thread reader, writer;
	
	public ReadWrite(InputStream inputStream, OutputStream outputStream, InputStream in, PrintStream out) {
		this.remoteInput = inputStream;
		this.remoteOutput = outputStream;
		this.localInput = in;
		this.localOutput = out;

		reader = new Thread () {
			public void run() {
				int ch;
				try {
					while ((!interrupted() && (ch = localInput.read()) != -1)) {
						remoteOutput.write(ch);
						remoteOutput.flush();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}//end-try
			}//end-run
		};//end-thread
		
		writer = new Thread () {
			public void run() {
				try {
					Util.copyStream(remoteInput, localOutput);
				} catch (IOException e) {
					e.printStackTrace();
				}//end-try
			}//end-run
		};//end-thread
		
		writer.start();
		reader.start();
	    try {
			writer.join();
			reader.interrupt();
		} catch (InterruptedException e) {
		}
		
	}//end-constructor
}//end-class
