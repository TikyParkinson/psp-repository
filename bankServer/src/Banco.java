
public class Banco {
	int maxCuentas = 10;
    Cuenta [] cuenta = new Cuenta[maxCuentas];
	
    public Banco() {
    	for (int i =0; i < maxCuentas; i++) {
    		cuenta[i] = new Cuenta();
    	}
    }
    public double saldo(int ncuenta) {
		return cuenta[ncuenta].getSaldo();
	}
	public boolean deposito(int ncuenta, double cantidad) {
		return cuenta[ncuenta].deposito(cantidad);
		
	}
	public boolean reintegro(int ncuenta, double cantidad) {
		return cuenta[ncuenta].reintegro(cantidad);
		
	}
}
