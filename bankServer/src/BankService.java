import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class BankService extends Thread{

	Socket s;
	Banco banco;
	Scanner in;
	PrintWriter out;
	
	public BankService(Socket s, Banco banco) {
		this.s = s;
		this.banco = banco;
		this.start();
	}
	
	public void run() {
		try {
			in = new Scanner(s.getInputStream());
			out = new PrintWriter(s.getOutputStream());
			out.println("Bienvenido a su banco online");
			out.println("----------------------------");
			out.println(">");
			out.flush();
			doService();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				s.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//end-try {
		
	}//end-run

	private void doService() {
		while(true) {
			if (!in.hasNext()) {
				return;
			}
			String comando = in.next();
			if (comando.equals("quit")) {
				return;
			}
			procesaComando(comando);
		}
	}

	private void procesaComando(String comando) {
		boolean errorCmd = true;
		if (comando.contains("sal")) {
			int cuenta = in.nextInt();
			double saldo = banco.saldo(cuenta);
			out.println(saldo);
			errorCmd = false;
		}
		if (comando.contains("dep")) {
			int cuenta = in.nextInt();
			double cantidad = in.nextDouble();
			boolean op=banco.deposito(cuenta,cantidad);
			if (op)
				out.println("deposito realizado con éxito, saldo: "+banco.saldo(cuenta));
			else
				out.println("No se ha podido realizar el depósito, saldo"+banco.saldo(cuenta));
			errorCmd = false;
		}
		if (comando.contains("ret")) {
			int cuenta = in.nextInt();
			double cantidad = in.nextDouble();
			boolean op = banco.reintegro(cuenta,cantidad);
			if (op) {
				out.println("reintegro realizado con éxito, saldo: "+banco.saldo(cuenta));
			}else {
				out.println("No hay saldo suficiente, saldo:"+banco.saldo(cuenta));
			}
			errorCmd = false;
		}
		if (errorCmd) {
			out.println("Comando erróneo!!!!!!");
		}
		out.println(">");
		out.flush();
	}//end-procesa-comando

}
